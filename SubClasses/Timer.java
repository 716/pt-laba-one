package SubClasses;

public class Timer {
    public double spawnTimer;
    public double startTimer;

    public Timer() {
        restart();
    }

    public void update() {
        spawnTimer = System.currentTimeMillis() - startTimer;
    }

    public void restart() {
        spawnTimer = 0;
        startTimer = System.currentTimeMillis();
    }
}