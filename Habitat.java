import javax.swing.*;

import SubClasses.Timer;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class Habitat {
    private ArrayList<BaseBuild> buildList = new ArrayList<BaseBuild>();
    private boolean isPaused = true;

    private int WIDTH;
    private int HEIGHT;

    private int woodTimerProgressValue;
    private int capitalTimerProgressValue;
    
    private JPanel habbitViewPanel = new JPanel();
    private JLayeredPane habbitViewLayeredPane = new JLayeredPane();

    private Timer woodTimer = new Timer();
    private Timer capitalTimer = new Timer();

    public void startHandle() {
        habbitViewLayeredPane.removeAll();
        buildList.clear();
        isPaused = true;
    }

    public void pauseHandler() {
        woodTimer.restart();
        capitalTimer.restart();
        isPaused = false;
    }

    public void repaint() {
        habbitViewLayeredPane.repaint();
    }

    public int getWoodProgress() {
        return woodTimerProgressValue;
    }

    public int getCapitalProgress() {
        return capitalTimerProgressValue;
    }

    public boolean getHabitatState(){
        return isPaused;
    }

    public Habitat(int _WIDTH, int _HEIGHT) {
        WIDTH = _WIDTH;
        HEIGHT = _HEIGHT;
    }

    public JPanel LayPane(){
        habbitViewLayeredPane.setBounds(0, 0, WIDTH - 200, HEIGHT - 100);
        habbitViewPanel.setLayout(null);
        habbitViewPanel.setBounds(150, 25, WIDTH - 200, HEIGHT - 100);
        habbitViewPanel.setBackground(Color.gray);
        habbitViewPanel.add(habbitViewLayeredPane);

        return habbitViewPanel;
    }
    
    public void update() {
    
        if (!isPaused) {
            Random myRand = new Random();
            double temp = 0;
            double Build_P = 0;

            woodTimerProgressValue = (int) (woodTimer.spawnTimer / 10 / WoodBuild.getN());
            capitalTimerProgressValue = (int) (capitalTimer.spawnTimer / 10 / CapitalBuild.getN());

            woodTimer.update();
            if (woodTimer.spawnTimer > WoodBuild.getN() * 1000) {
                Build_P = WoodBuild.getP();
                temp = myRand.nextDouble();
                if (temp < Build_P) {
                    addObj(new WoodBuild());
                }
                woodTimer.restart();
            }

            capitalTimer.update();
            if (capitalTimer.spawnTimer > CapitalBuild.getN() * 1000) {
                Build_P = CapitalBuild.getP();
                temp = myRand.nextDouble();
                if (temp < Build_P) {
                    addObj(new CapitalBuild());
                }
                capitalTimer.restart();
            }

        } else {
            if (woodTimer.spawnTimer != 0)
                woodTimer.restart();
            if (capitalTimer.spawnTimer != 0)
                capitalTimer.restart();
        }
    }
    


    private void addObj(BaseBuild obj) {
        Random myRand = new Random();
        obj.setX(myRand.nextInt(WIDTH - 200 - obj.getWidth()));
        obj.setY(myRand.nextInt(HEIGHT - 100 - obj.getHeight()));
        buildList.add(obj);

        habbitViewLayeredPane.add(obj.label, 0, 0);
        habbitViewLayeredPane.repaint();
    }
}