import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class App extends JFrame {
    private static final long serialVersionUID = 1L;
    private JButton buttonStart = new JButton("Start");
    private JSlider slider = new JSlider();
    
    private JProgressBar prBar_WoodBuild = new JProgressBar();
    private JProgressBar prBar_CapitalBuild = new JProgressBar();
    
    /* Дизайн частично адаптирован
       под изменение размеров окна */
    public final int WIDTH = 1280;
    public final int HEIGHT = 720;

    private Habitat habitat = new Habitat(WIDTH, HEIGHT);

    public App() {
        super("Hi!");
        this.setBounds(100, 100, WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        Butlist butlist = new Butlist();
        Slidlist slidlist = new Slidlist();
        Log log = new Log();
        buttonStart.addActionListener(butlist);
        slider.addChangeListener(slidlist);


        Container container = this.getContentPane();
        container.setLayout(null);
        buttonStart.setBounds(5, 50, 130, 35);
        buttonStart.setBackground(Color.GREEN);
        container.add(buttonStart);

        JTextArea textChanceToCreate = new JTextArea(" Вероятность появления\n деревянного домика");
        textChanceToCreate.setAlignmentY(0.5f);
        textChanceToCreate.setBounds(0, 155, 150, 40);
        container.add(textChanceToCreate);
        
        slider.setBounds(5, 205, 130, 35);
        slider.setPaintLabels(true);
        slider.setMajorTickSpacing(20);
        container.add(slider);

        prBar_WoodBuild.setString("Wood build");
        prBar_WoodBuild.setStringPainted(true);
        prBar_WoodBuild.setBounds(5, 250, 140, 50);
        container.add(prBar_WoodBuild);

        prBar_CapitalBuild.setString("Capital build");
        prBar_CapitalBuild.setStringPainted(true);
        prBar_CapitalBuild.setBounds(5, 350, 140, 50);
        container.add(prBar_CapitalBuild);

        container.add(habitat.LayPane());

        log.setBounds(5, 450, 130, 130);
        container.add(log);
    }

    public void run()
    {
        while (JFrame.getFrames() != null) {
            habitat.update();
            
            prBar_WoodBuild.setValue(habitat.getWoodProgress());
            prBar_CapitalBuild.setValue(habitat.getCapitalProgress());
        }
    }
    
    private class Butlist implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (habitat.getHabitatState() == true) {
                buttonStart.setText("Pause");
                buttonStart.setBackground(Color.RED);
                habitat.pauseHandler();
            } else {
                buttonStart.setText("Start");
                buttonStart.setBackground(Color.GREEN);
                habitat.startHandle();
            }
            habitat.repaint();
        }
    }

    //TODO: Нужно передавать как-то ССЫЛКУ на переменную в конструктор.
    private class Slidlist implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            int value = ((JSlider) e.getSource()).getValue();
            WoodBuild.setP(value / 100.0);
        }
    }

}
