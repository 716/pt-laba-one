
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Log extends JPanel{
    private static final long serialVersionUID = 1L;
    private JLabel topLabel = new JLabel("LOG:   ");
    private Font font = new Font("Verdana", Font.PLAIN, 12);
    
    Log(){
        setLayout(new GridLayout(1, 1));
        setBackground(Color.gray);
        topLabel.setVerticalAlignment(JLabel.TOP);
        topLabel.setHorizontalAlignment(JLabel.LEFT);
        topLabel.setPreferredSize(getSize());
        topLabel.setFont(font);
        topLabel.setForeground(Color.GREEN);
        add(topLabel);
    }

}