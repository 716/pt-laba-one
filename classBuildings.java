
import javax.swing.*;

interface IBehavior
{
    void setX(int x);
    void setY(int y);
    int getX();
    int getY();

    void move(int dx, int dy); //смещение относительно текущей позиции

}

abstract class BaseBuild implements IBehavior {
    private int _x,
                _y;
                

    private ImageIcon _image;
    private int Width;
    private int Height;
    public JLabel label = new JLabel();

    public void setX(int x)
    {
        _x = x;
        label.setBounds(_x, _y, Width, Height);
    }

    public void setY(int y)
    {
        _y = y;
        label.setBounds(_x, _y, Width, Height);
    }

    public int getX()
    {
        return _x;
    }

    public int getY()
    {
        return _y;
    }

    public int getWidth(){
        return Width;
    }

    public int getHeight(){
        return Height;
    }

    public void move(int dx, int dy) {
        _x += dx;
        _y += dy;
        label.setBounds(_x, _y, Width, Height);
    }

    public BaseBuild(String path){
        _x = 0;
        _y = 0;
        label.setBounds(_x, _y, Width, Height);
        loadImage(path);
        label.setIcon(_image);
    }
    public BaseBuild(String path, int x, int y){
        _x = x;
        _y = y;
        label.setBounds(_x, _y, Width, Height);
        loadImage(path);
    }

    private void loadImage(String path)
    {
        try
        {
            _image = new ImageIcon(path);
        } catch (Exception e) {
            //TODO: handle exception
        }
        Width = _image.getIconWidth();
        Height = _image.getIconHeight();
    }
}

class CapitalBuild extends BaseBuild {
    
    private static double _P = 0.5,
                          _N = 3;
    public CapitalBuild(){
        super("src/img/capitalHouse.png");
    }
    

    public CapitalBuild(int x, int y){
        super("src/img/CapitalHouse.png", x, y);
    }
    
    public static double getN(){
        return _N;
    }

    public static double getP(){
        return _P;
    }

    public static void setN(double N){
        _N = N;
    }

    public static void setP(double P){
        _P = P;
    }

}



class WoodBuild extends BaseBuild {
    
    private static double _P = 0.5,
                          _N = 2;
    public WoodBuild(){
        //this(500, 0);

        super("src/img/woodHouse.png");
    }

    public WoodBuild(int x, int y){
        super("src/img/woodHouse.png", x, y);
    }
    
    public static double getN(){
        return _N;
    }

    public static double getP(){
        return _P;
    }

    public static void setN(double N){
        _N = N;
    }

    public static void setP(double P){
        _P = P;
    }
}